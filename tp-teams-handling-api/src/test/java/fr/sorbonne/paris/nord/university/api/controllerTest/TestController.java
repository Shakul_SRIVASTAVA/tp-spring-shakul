package fr.sorbonne.paris.nord.university.api.controllerTest;


import fr.sorbonne.paris.nord.university.api.controller.TeamController;
import fr.sorbonne.paris.nord.university.api.entity.TeamEntity;
import fr.sorbonne.paris.nord.university.api.mapper.TeamMapper;
import fr.sorbonne.paris.nord.university.api.service.TeamService;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.mockito.Mockito.when;

@SpringBootTest
public class TestController {
/*
    @Mock
    private TeamService teamService;

    @Autowired
    private TeamMapper teamMapper;

    @BeforeEach
    public void initialiseRestAssuredMockMvcStandalone() {
        RestAssuredMockMvc.standaloneSetup(new TeamController(teamService,teamMapper));
    }
    private final TeamEntity e1 = new TeamEntity(1L,"Arsenal", "Gonners !!!!! ");
    private final TeamEntity e2 = new TeamEntity(2L,"Totenham", "We are Losers");
    private final TeamEntity e3 = new TeamEntity(3L,"Man U", "Red Losers ");
    private final TeamEntity e4 = new TeamEntity(4L,"Liverpool", "Walk Alone");
    private final TeamEntity e5 = new TeamEntity(5L,"Barça", "Messi !! Messi !! ");

    @Test
    public void shouldReturnStatus200_WhenGetTeamsTestWorks() {

        when(teamService.findAllTeams()).thenReturn(List.of(e1,e2,e3,e4,e5));
        given()
                .when()
                .get("teams")
                .then()
                .assertThat()
                .statusCode(200);

    }

    @Test
    public void shouldReturnStatus200_WhenGetTeamByIdTestWorks() {

        when(teamService.findTeamWithId(4L)).thenReturn(e4);
        given()
                .when()
                .get("teams/{id}",4L )
                .then()
                .statusCode(200);
    }

    @Test
    public void shouldReturnStatus200_WhenDeleteTeamWork(){

        given().contentType("application/json").when().delete("/deleteTeamWithId/{id}",2l).then().statusCode(200);
    }

 */
}
