package fr.sorbonne.paris.nord.university.api;

import fr.sorbonne.paris.nord.university.api.entity.TeamEntity;
import fr.sorbonne.paris.nord.university.api.service.TeamService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
public class TeamServiceTest {

    @Autowired
    private TeamService teamService;

    @Test
    public  void shouldReturnArsenal_whenFindTeamWithIDCalled(){

        //given
        Long arsenalTeamID = 1l;

        //when
        TeamEntity responseFindTeamWithId = teamService.findTeamWithId(arsenalTeamID);

        //then
        assertThat(responseFindTeamWithId.getName()).isNotNull().isEqualTo("Arsenal");

    }

    @Test
    public void shouldReturnAllTeams_whenFindAllTeamsCalled() {

        List<TeamEntity> teams = teamService.findAllTeams();

        assertThat(teams).isNotNull().isNotEmpty();
    }

    @Test
    public void givenManUTeamId_whenDeleteTeamById() {

        // Given.
        Long ManUTeamId = 5L;
        int listSize = teamService.findAllTeams().size();
        // When.
        teamService.deleteTeamWithId(ManUTeamId);
        // Then.
        assertThat(teamService.findAllTeams().size()).isNotNull().isEqualTo(listSize-1);
    }

    @Test
    public void shouldAddNewTeam_WhenAddTeamCalled() {

        // Given.
        TeamEntity om = new TeamEntity(6L,"OM","Droit au but");
        int listSize = teamService.findAllTeams().size();
        // When.
        teamService.addTeam(om);
        // Then.
        assertThat(teamService.findAllTeams().size()).isNotNull().isEqualTo(listSize+1);
    }
    

}
