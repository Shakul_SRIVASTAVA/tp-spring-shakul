package fr.sorbonne.paris.nord.university.api.mapper;

import fr.sorbonne.paris.nord.university.api.controller.TeamDto;
import fr.sorbonne.paris.nord.university.api.entity.TeamEntity;
import org.springframework.stereotype.Component;

@Component
public class TeamMapper {

    public TeamDto teamEntityToTeamDto(TeamEntity t)
    {
        return  new TeamDto(t.getId(),t.getName(),t.getSlogan());
    }

    public TeamEntity TeamDtoToTeamEntity(TeamDto t)
    {
        return  new TeamEntity(t.getId(),t.getName(),t.getSlogan());
    }
}
