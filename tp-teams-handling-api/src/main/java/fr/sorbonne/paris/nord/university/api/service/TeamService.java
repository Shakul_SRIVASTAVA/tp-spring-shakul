package fr.sorbonne.paris.nord.university.api.service;

import fr.sorbonne.paris.nord.university.api.entity.TeamEntity;
import fr.sorbonne.paris.nord.university.api.myTeamException.TeamException;
import fr.sorbonne.paris.nord.university.api.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TeamService {
   private  final TeamRepository teamRepository;

    @Autowired
    public TeamService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }


    public List<TeamEntity> findAllTeams(){
        return teamRepository.findAll();
    }

    public TeamEntity findTeamWithId(Long id)  {
        TeamEntity t =  teamRepository.findById(id).orElse(null); // findbyID rend un optional
       /* if(t.getName() == null || t.getName()==""|| t.getSlogan()==""||t.getSlogan()==null)
        {
            throw new TeamException("Remplir correctement les champs slogan et name");
        }*/
        return  t;
    }

    @Transactional
    public void deleteTeamWithId(Long id)
    {
        teamRepository.deleteById(id);
    }
    @Transactional
    public TeamEntity addTeam(TeamEntity team)
    {
        return teamRepository.save(team);
    }

    @Transactional
    public TeamEntity updateTeam(TeamEntity team)
    {
        return teamRepository.save(team);
    }








}
