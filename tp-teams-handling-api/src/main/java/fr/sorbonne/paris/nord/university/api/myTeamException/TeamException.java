package fr.sorbonne.paris.nord.university.api.myTeamException;

public class TeamException extends Exception{

    public TeamException(){
        super();
    }
    public TeamException(String s){
        super(s);
    }
}
