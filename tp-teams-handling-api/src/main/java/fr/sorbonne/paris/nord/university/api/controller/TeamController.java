package fr.sorbonne.paris.nord.university.api.controller;


import fr.sorbonne.paris.nord.university.api.entity.TeamEntity;
import fr.sorbonne.paris.nord.university.api.mapper.TeamMapper;
import fr.sorbonne.paris.nord.university.api.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class TeamController {

    private final TeamService teamService;
    private   final  TeamMapper teamMapper;

    @Autowired
    public TeamController(TeamService teamService, TeamMapper teamMapper) {
        this.teamService = teamService;
        this.teamMapper = teamMapper;
    }
    /*
    @GetMapping("/hello")
    public  String getTeams(){
        return "Hello World";
    }*/

    @GetMapping("/teams")
    public List<TeamDto> getTeams(){

        return teamService.findAllTeams().stream().map(teamEntity -> teamMapper.teamEntityToTeamDto(teamEntity)).collect(Collectors.toList());
    }
    @RequestMapping("/teams/{id}")
    public TeamDto getTeamById(@PathVariable(value = "id") Long id) {
        return teamMapper.teamEntityToTeamDto(teamService.findTeamWithId(id));
    }

    @PostMapping("/createTeam")
    public TeamDto createTeam(@RequestBody TeamDto teamDto){
        TeamEntity teamEntity = teamMapper.TeamDtoToTeamEntity(teamDto);
        return teamMapper
                .teamEntityToTeamDto(teamService.addTeam(teamEntity));
    }

    @PutMapping("/modifyTeamWithId/{id}")
    public TeamDto modifyTeamById(@RequestBody TeamDto teamDto, @PathVariable(value = "id") Long id) {
        TeamEntity teamEntity = teamMapper.TeamDtoToTeamEntity(teamDto);

        return teamMapper.teamEntityToTeamDto(teamService.updateTeam(teamEntity));

    }

    @DeleteMapping("/deleteTeamWithId/{id}")
    public void deleteTeam(@PathVariable(value = "id") Long id) {
        teamService.deleteTeamWithId(id);
    }

}
